from setuptools import setup

setup(name='pyfoamutils',
      version='0.1',
      description='Utilities for pre- and post- processing of channel flow simulations in OpenFOAM',
      url='https://gitlab.com/lhcamilo/myfoamutils.git',
      author='Leonardo Honfi Camilo',
      author_email='lhcamilo@gmail.com',
      license='MIT',
      install_requires=['numpy', 'scipy'],
      packages=['pyfoamutils'],
      zip_safe=False)
