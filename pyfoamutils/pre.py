#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: leonardo honfi camilo
"""
import numpy as np
import os
import pyfoamutils.blockMeshStrings as bms

def channelVelocityCorr(retau, nu, mode = 'mixed', h = 1,re_precision = 0, vel_precision = 3):
    """
    Computes the bulk velocity and reynolds number for a given fricional
    reynolds number in a cyclic poiselle flow channel. 
    Returns a tupple of the Bulk reynolds number and the bulk velocity
    
    It is assumed the half channel height, h, is equal to 1.
    
    The velocity is output in metres per second.

Input:
    retau (int/float):      Frictional Reynolds number
    nu (float):             Kinematic viscosity
    mode (string):          Choose between Pope's, Dean's correlations or the
                            average of the two correlations. Default = 'mixed'
    h (float):              Half-Channel height. Default = 1
    re_precision (int):     Precision of the output bulk reynolds. Default = 0
    vel_precision (int):    Precision of the output bulk velocity. Default = 3
Output:
    Re_bulk (float):        Bulk Reynolds number
    vel_bulk (float):       Bulk velocity
    """
    
    Rbp = (retau/0.09)**(100/88)
    Rbd = 2.0*(retau/0.175)**(8.0/7.0) 
    Ubp = (nu*Rbp)/(2.0*h)
    Ubd = (nu*Rbd)/(2.0*h)
    
    if mode == 'mixed' :
        return (round(0.5*(Rbp + Rbd), re_precision),
                    round(0.5*(Ubp + Ubd),vel_precision))
    elif  mode == 'dean':
        return round(Rbd,re_precision), round(Ubd,vel_precision)
    elif mode == 'pope':
        return round(Rbp,re_precision), round(Ubp,vel_precision)


def plus2Real(retau, plusUnits, h = 1):
    '''
Converts plus/frictional units to metric/real units

    y = y+ * h/Re_tau

Input: 
    retau (int):        Frictional Reynolds number
    plusUnits (float):  Frictional/plus units   
    h (float):          Half-channel height, assumed to be 1 by default
    
Output (float):         Real (metric) units
 
    '''
    return plusUnits*h/retau


def real2Plus(retau, realUnits, h = 1):
    '''
Converts plus/frictional units to metric/real units

    y+ = Re_tau*y/h

Input: 
    retau (int):        Frictional Reynolds number
    realUnits (float):  Real (metric) units   
    h (float):          Half-channel height, assumed to be 1 by default
    
Output (float):         Frictional/plus units
    '''

    return realUnits*retau/h

def checkListLength(someList, length):
    """
Small test function that tests the length of lists/arrays and raises an 
exception if the expected length is not found

Intput: someList (list):    Generic list to be tested

Output: void:   raises exception
    """
    assert len(someList) == length, ("Imput list should"+
            " have {} elements, found: {}".format(length, len(someList)))

def readListInputs(someList):
    """
Reads input lists  of varying shapes and outputs and a (3, 2) numpy array.
Single element rows are expanded to 2 with the same element broadcast through
the row.

Input: someList (list): Generic coordinate/cellsize input list

Output: (3, 2) numpy array 
    """
    checkListLength(someList, 3)
    resultArr = np.zeros((3,2), dtype=float)
    for item_ind, item in enumerate(someList):
        
        if isinstance(item, (float, int)):
            resultArr[item_ind,:] = np.ones((1,2))*item
        
        elif isinstance(item, (dict, set)):
            raise TypeError('List element must be an int, float,'+
                            ' list or tuple')
            
        else:
            checkListLength(item, 2)
            resultArr[item_ind,:] = np.array(item)
            
    return resultArr

def calcExpansionLength(nExpansion, lMin, ratio):
    """
Computes the length of an (geometric) expansion region/layer.

Input: 
    nExpansion (int):   Number of cells in the expansion region/layer
    lMin (float):       Minimum prescribed cell length near the wall.
    ratio (float):      Prescribed ratio between neighbouring cells in the 
                        expansion region/layer.

Output: (float)         Length of the expansion region/layer 
    """
    return lMin*(1-(ratio**(nExpansion)))/(1-ratio);

def calcExpansionCellCount(lMin, lMax, ratio):
    """
Estimate the number of cells in a expansion region/layer
    
Input:
    
    lMin (float):       Minimum prescribed cell length near the wall.
    lMax (float):       Maximum cell length in a expansion region/layer.
    ratio (float):      Prescribed ratio between neighbouring cells in the 
                        expansion region/layer.

Output: (int)           Total number of cells in the expansion region
        
    """
    
# Total number of cells in the expansion region:
    
    return 1 + np.floor(np.log(lMax/lMin)/np.log(ratio))
 

def calcUniformLength(domainLength, expLength, symmetry):
    """
Computes the length of an uniform region depending on the length of the 
expansion layer and symmetry condition

Input:
    
    domainLength (float):   Length of the entire domain/region 
    expLength (float):      Length of the expansion region/layer
    symmetry (boolean):     Switch to determine if there is symmetry about
                            the mid point of the domain.

Output: (float)             Length of the uniform region/layer
    
    """
    if symmetry:
        return domainLength - 2 * expLength
    else: 
        return domainLength - expLength

def calcUniform(domainLength, lMax):
    """ 
Computes the number of cells and cell length inside an uniform region

Input:        
    domainLength (float):   Length of the entire domain/region 
    lMax (float):           Maximum cell length in a expansion region/layer.
Output:
    nUniform (int):     Number of cells in the uniform region/layer
    luniform (float):   Length of the uniform region/layer
    """
    nUniform = np.round(domainLength/lMax)
    lUniform = domainLength/nUniform
    return nUniform, lUniform
    
def calcExpansion(lMin, lMax, domainLength, ratio, symmetry = False, 
                  debug = False):
    """
Computes the length of the expansion region. 
Checks if the length is not larger than the midpoint of the channel (symmetry) 
or the edges of the domain and reduces the number of cells accordingly
Checks the ratio between the lengths of the largest expansion cell and the 
neighbouring uniform region cell so it does not exceed the prescribed expansion
ratio, maintaining the quality of the mesh.

Input:

    lMin (float):       Minimum prescribed cell length near the wall.
    lMax (float):       Maximum cell length in a expansion region/layer.
    domainLength (float):   Length of the entire domain/region 
    ratio (float):      Prescribed ratio between neighbouring cells in the 
                        expansion region/layer.
    symmetry (boolean):     Switch to determine if there is symmetry about
                            the mid point of the domain.
    debug (boolean):    Switch to print metrics for debugging

    
    """
    
    nExpansion = calcExpansionCellCount(lMin, lMax, ratio)
    expLength = calcExpansionLength(nExpansion, lMin, ratio)
    
    if debug: print(1, nExpansion, expLength, symmetry)
    
    if symmetry:
        limitLength = 0.5*domainLength
    else:
        limitLength = domainLength
        
    while expLength > limitLength:
        nExpansion -= 1
        expLength = calcExpansionLength(nExpansion, lMin, ratio)
        #update the maximum cell size
        lMax = lMin*(ratio**(nExpansion-1))
        
    uniformLength = calcUniformLength(domainLength, expLength, symmetry)
    
    # Tried to solve the situation where a division by zero occurs when
    # nUniform = 0, not ideal but...good enough for now.
    # revise
    
    if uniformLength < lMax*0.5/ratio:
        nExpansion -= 1
        expLength = calcExpansionLength(nExpansion, lMin, ratio)
        #update the maximum cell size
        lMax = lMin*(ratio**(nExpansion-1))
        uniformLength = calcUniformLength(domainLength, expLength, symmetry)

        
    #if uniformLength < 0.9*lMax:
        
    if debug: print(2, lMax, expLength, domainLength, uniformLength)
    # number of cells in the uniform region

    nUniform, lUniform = calcUniform(uniformLength, lMax)
    if debug: print (3, nUniform, lUniform)
    
    if symmetry:
        nAggregate =  nUniform + 2 *  nExpansion
    else:
        nAggregate =  nUniform + 2 *  nExpansion
        
        
    interRatio = lMax/lUniform
    
    while (interRatio < (1/ratio) or interRatio > ratio):
            
        nExpansion -= 1
        expLength = calcExpansionLength(nExpansion, lMin, ratio)
        lMax = lMin*(ratio**(nExpansion-1))
        
        uniformLength = calcUniformLength(domainLength, expLength, symmetry)
            
        nUniform, lUniform = calcUniform(uniformLength, lMax)
        
        if debug: print (4, nUniform, lUniform)

     
        interRatio = lMax/lUniform
        
    stretchRatio = lMax/lMin
        
        
    return [nExpansion, nUniform, nAggregate], [lMin, lMax, lUniform], \
    [expLength, lUniform], interRatio, stretchRatio
         
# =============================================================================
# CLASSES    
# =============================================================================

class CoordinatePair():
    def __init__(self, pair):
        self.min = min(pair)
        self.max = max(pair)
        self.values = pair

class Units():
    def __init__(self, unit):
        self.x = CoordinatePair(unit[0,:])
        self.y = CoordinatePair(unit[1,:])
        self.z = CoordinatePair(unit[2,:])
        self.values = unit

class Ratios():
    
    def __init__(self, ratios):
        self.x = ratios[0]
        self.y = ratios[1]
        self.z = ratios[2]
        self.values = np.array(ratios)
        
class Cells():
    """
    Input: Numpy array, (3,2),  of cell sizes
    """
    def __init__(self, cells, retau, h, frictional = True):
        if frictional:
            self.frictional = Units(cells)
            self.real = Units(cells*h/retau)
        else:
            self.frictional = Units(cells*retau/h)
            self.real = Units(cells)
class Domain():
    
    def __init__(self, domain):
                
        self.values = domain
        self.x = CoordinatePair(domain[0,:])
        self.y = CoordinatePair(domain[1,:])
        self.z = CoordinatePair(domain[2,:])
        self.min = domain[:,0]
        self.max = domain[:,1]
        
        self.length = self.x.max - self.x.min
        self.height = self.y.max - self.y.min
        self.width = self.z.max - self.z.min
        self.h = self.height*0.5
        

        
class OneDimGrid():
    """
    """
    def __init__(self, ratio, cell, domain, symmetry=False):
        self.ratio = ratio
        self.cell = cell
        self.domain = domain
        self.length = max(domain) - min(domain)
        
        if ratio == 1:
            self.cellCount, self.cellLength = calcUniform(self.length,
                                                               max(cell))
        else:
            self.cellCount, self.cellLengths, self.lengths ,\
            self.interFaceRatio, self.stretchRatio = \
            calcExpansion(min(cell),  max(cell) , self.length, ratio,
                          symmetry, debug = False)

            
class Channel():
    """
    Creates a channel object 
    prints the blockMeshDict File to be read by the blockMesh utility
    """
    def showBlockMeshDict(self):
        pass
           
    def _updateCells(self):
        
        cellUpdate = [self.streamwise.cellLength,
         [min(self.vertical.cellLengths), max(self.vertical.cellLengths)],
                                                 self.spanwise.cellLength]
        
        #print('\n\tUpdating cell sizes.\n')
        
        self.cells = Cells(readListInputs(cellUpdate), self.retau,
                           self.domain.h, frictional = False)
        

    def _generateGrids(self):
        
        #print("\n\tGenerating grids.\n")
                
        self.streamwise = OneDimGrid(self.ratios.x, self.cells.real.x.values,
                                     self.domain.x.values)
        self.vertical = OneDimGrid(self.ratios.y, self.cells.real.y.values,
                                     self.domain.y.values, symmetry = True)
        self.spanwise = OneDimGrid(self.ratios.z, self.cells.real.z.values,
                                     self.domain.z.values)
    def _generateCount(self):
        self.cellCount = [self.streamwise.cellCount,
                         self.vertical.cellCount,
                          self.spanwise.cellCount]
        
    def _generateHeader(self):
        X = self.domain.x.max
        Y1 = self.vertical.lengths[0]
        Y2 = Y1 +  self.vertical.lengths[1]
        Y3 = self.domain.y.max
        Z = self.domain.z.max
        retau = self.retau
        cellCount = self.cellCount
        cellSizes = [self.cells.frictional.x.max, 
                 [self.cells.frictional.y.min, self.cells.frictional.y.max],
                 self.cells.frictional.z.max]
        
        stretchRatio = self.vertical.stretchRatio
        
        lengths = [X, [ Y1, Y2, Y3 ], Z]
        
        self.blockHeader = bms.blockMeshHeader(retau, cellCount, cellSizes,
                            lengths, stretchRatio, self.fixedMass)
    
    def _generateDictionary(self, fixedMass):
        
        blockMeshTop, blockMeshBottom =  bms.readChannelBlockMesh(fixedMass)
        
        return blockMeshTop + '\n' + self.blockHeader + '\n' + blockMeshBottom
    

    
    def __init__(self, retau, domain, cellSizes, ratios, fixedMass = False):
        
        self.retau = retau
        self.domain = Domain(readListInputs(domain))
        self.cells = Cells(readListInputs(cellSizes), retau, self.domain.h)
        self.ratios = Ratios(ratios)
        self.fixedMass = fixedMass
        

        # Generate grids
        self._generateGrids()

        # Update cell sizes
        self._updateCells()
        
        # Count Cells
        self._generateCount()
        # Generate blockMesh headers
        self._generateHeader()
        # Generate Dictionary
        self.blocMeshDict = self._generateDictionary(self.fixedMass)
        
#    def __str__(self):
#        print(self.blockHeader)
        
    def __repr__(self):
        
        return self.blockHeader
        
        
    def write(self, location= os.getcwd(),
              fileName='blockMeshDict' ):
        
        with open(os.path.join(location, fileName),"w") as file:
            file.write(self.blocMeshDict)

        
        

            

class ConjugateChannel(Channel):
    """
    """
    def _generateGrids(self):
        #print('wall')
        self.wall = OneDimGrid(self.ratios.y, self.cells.real.y.values,
                                     [0.0, self.wallHeight], symmetry = False)
        #print('done with wall')
        super(ConjugateChannel, self)._generateGrids()
        
    def _generateCount(self):
        self.cellCount = [self.streamwise.cellCount,
                         self.vertical.cellCount,
                          self.spanwise.cellCount,
                          self.wall.cellCount]
    
    def _generateHeader(self):
        X = self.domain.x.max
        Y1 = self.vertical.lengths[0]           # height of the bottom BL
        Y2 = Y1 +  self.vertical.lengths[1]     # height of top BL
        Y3 = self.domain.y.max                  # height of the channel
        Y4 = Y3 + self.wall.lengths[0]          # height of the top wall BL
        Y5 = Y3 + self.wallHeight               # height of the top wall
        Y6 = -self.wall.lengths[0]              # depth of the lower wall BL
        Y7 = -self.wallHeight                   # depth of the lower wall BL
        Z = self.domain.z.max
        retau = self.retau
        cellCount = self.cellCount
        cellSizes = [self.cells.frictional.x.max, 
                    [self.cells.frictional.y.min, self.cells.frictional.y.max],
                    self.cells.frictional.z.max]
        
        stretchRatio = [self.vertical.stretchRatio, self.wall.stretchRatio]

        
        lengths = [X, [ Y1, Y2, Y3, Y4, Y5, Y6, Y7 ], Z]
        
        self.blockHeader = bms.blockMeshConjHeader(retau, cellCount, cellSizes,
                            lengths, stretchRatio, self.fixedMass)
        
        def _generateDictionary(self, fixedMass):
        
            blockMeshTop, blockMeshBottom =  \
                            bms.readConjChannelBlockMesh(fixedMass)
        
            return blockMeshTop + '\n' + self.blockHeader + '\n' + \
                   blockMeshBottom

        
    def __init__(self, retau, domain, cellSizes, ratios, wallHeight=1):

        self.wallHeight = wallHeight
        super(ConjugateChannel, self).__init__(retau, domain, cellSizes,
                                               ratios)
        
     
if __name__ == '__main__':
    domain = [[0, 6.4], [0, 2], [0, 3.2]]
    cells = [90, [0.5, 30], 30]
    ratios = [1, 1.05, 1]
    grid90 = Channel(180, domain, cells, ratios)
    conjGrid90 = ConjugateChannel(180, domain, cells, ratios, wallHeight=0.5)

    retau = 180
    A = calcExpansion(plus2Real( retau, 0.5), plus2Real( retau, 30), 6.4, 1.05,
                      symmetry = False)
    
 
#class BFS(Channel):
    
    
