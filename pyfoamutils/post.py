#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: Leonardo Honfi Camilo
@email: lhcamilo@gmail.com
"""
import numpy as np 
import os
#from numba import jit

    
    
#-----------------------------------------------------------------------------   
# COUNT FUNCTIONS
def readGridHeader(caseLocation = os.getcwd()):
#    if caseLocation.startswith('/'):
    ownerLocation = os.path.join(caseLocation,'constant','polyMesh','owner')
#    else:
#        ownerLocation = os.path.join(os.getcwd(),caseLocation,'constant','polyMesh','owner')

    findNote = False
    gridheader = {}
    with open(ownerLocation) as file:
        for line in file:
            if line == '{\n':
                print('found')
                findNote = True
                continue
            if line == '}\n':
                break
            if findNote:
                if line.split()[0] == 'note':
                    line = line[:-3].split()[1:]
                    keys = ['points', 'cells', 'faces' , 'intFaces']
                    
                    if len(line) > len(keys):

                        gridheader = {key: int(value) for key, value in zip(keys,[entry for entry_ind, entry in enumerate(line) if entry_ind%2 == 1]) }
                    else:
                        
                        for entry_ind, entry in enumerate(line):
                            gridheader[keys[entry_ind]] = int(entry.split(':')[-1])
                            
                    return gridheader


                
def readFieldHeader(fieldLocation):
    headerDict = {}
    readHeader = False
    readCount = False
    with open(fieldLocation) as file:
        for line in file:
            # look for the header info
            if line.startswith('{\n'):
                readHeader = True
                continue
            if line.startswith('}\n'):
                readHeader = False
            if readHeader:
                entry = line[:-2].replace('"', '').split()
                # cosmetic changes
                if entry[0] == 'location': entry[0] = 'time'
                if entry[0] == 'object': entry[0] = 'field'
                if entry[0] == 'class': entry[0] = 'fieldType'
                headerDict[entry[0]] = entry[1]
            
            # look for the dimenions 
            if line.startswith('dimensions'):
                # convoluted but it works...
                entry = \
                list(map(int,line[:-2].split('[')[1].replace(']','').split()))
                headerDict['dimensions'] = entry
            # look for the cell count    
            if line.startswith('internalField'):
                readCount = True
                continue           
            if readCount:
                headerDict['cells'] = int(line)
                break
    return headerDict
    
def fetchPointCount(location):     
    """
    Returns the pointCount of a given openfoam field
    input: location of the file (string)
    ouput: cell count (integer)
    """
    findPointCount =  False
    with open(location) as file:
        for ind, line in enumerate(file):
            if line.endswith('//\n'):
                findPointCount = True
                continue
            if findPointCount:
                if line.startswith('\n'):
                    continue
                else:
                    return int(line)
              
def fetchFaceCount(location):
    """
    Returns the cellCount of a given openfoam field
    input: location of the file (string)
    ouput: cell count (integer)
    """
    findFaceCount =  False
    with open(location) as file:
        for ind, line in enumerate(file):
            if line.endswith('//\n'):
                findFaceCount = True
                continue
            if findFaceCount:
                if line.startswith('\n'):
                    continue
                else:
                    return int(line)

def fetchCellCount(faceCount, location):        
    """
    Returns the cellCount :of a given openfoam field
    input: location of the file (string)
    ouput: cell count (integer)
    """
    findOwnerList =  False
    ownerArr = np.empty(faceCount, dtype=int)
    with open(location) as file:
        for line in file:
            if line == '(\n':
                findOwnerList = True
                ind = 0
                continue
            if line == ')\n':
                break
            if findOwnerList:
                ownerArr[ind] = int(line)
                ind += 1
        return len(np.unique(ownerArr))                    
#-----------------------------------------------------------------------------
# FETCH ARRAY FUNCTIONS
    
def fetchPointArr(pointCount, location='constant/polyMesh/points'):
    """
    Reads the points file at constant/polyMesh/ and outputs a numpy array
    containing the coordinate of each point.
    
    In order to alocate the memory for the array the function takes the
    pointcount as an input.
    """    
    storage =  np.empty((pointCount, 3), dtype=float)
    findPointList = False
    with open(location) as file:
        for line in file:
            if line == '(\n':
                findPointList = True
                ind = 0
                continue
            if line == ')\n':
                break
            if findPointList:
                storage[ind, :] = np.fromstring(line[1:-1], dtype=float,
                       sep=' ')
                ind += 1
        return storage

def fetchFaceArr(faceCount, location = 'constant/polyMesh/faces'):           
    """
    Reads the faces file at constant/polyMesh/ and outputs a numpy array 
    containing lists of each point at the corner of each face. Since the 
    faces are quadrilateral each list is composed of 4 points and the array
    space can be readily pre-alocated. 
    """    
    storage =  np.empty((faceCount, 4), dtype=int)
    findFaceList = False
    with open(location) as file:
        for line in file:
            if line == '(\n':
                findFaceList = True
                ind = 0
                continue
            if line == ')\n':
                break
            if findFaceList:
                storage[ind, :] = np.fromstring(line[2:-2], dtype=int,
                       sep=' ')
                ind += 1
        return storage    

# CALC ARRAY FUNCTIONS

def calcCellFaceArr(faceCount, cellCount, 
                    location='constant/polyMesh/owner'):        
    """
    Returns the cellCount of a given openfoam field
    input: location of the file (string)
    ouput: cell count (integer)
    """
    # Generate storage np.array
    storage = np.empty(cellCount, dtype = object)
    for ind in range(cellCount):
        storage[ind] =  []
    
    findOwnerList =  False
    with open(location) as file:
        for line in file:
            if line == '(\n':
                findOwnerList = True
                ind = 0
                continue
            if line == ')\n':
                break
            if findOwnerList:
                storage[int(line)].append(ind)
                ind += 1
        return storage
            
#@jit
def calcFacePointArr(faceArr, pointArr):
    """
    Maps the array containing the list of points in each face, faceArr, and
    matches each point with its repective coordinate contained in the point
    array, pointArr. Each entry of th faceArr lists corresponds to the
    index of the point Array
    """
    storage = np.empty((len(faceArr), 3), dtype = float)
    tempStorage = np.empty((4, 3), dtype = float)
    
    for face_ind, face in enumerate(faceArr):
        
        for ind in range(4):
            tempStorage[ind,:] = pointArr[face[ind],:]
        storage[face_ind,:] = 0.5*(tempStorage.min(axis=0) +
                                   tempStorage.max(axis=0))
        
    return storage
    

def calcCellArr(cellFaceArr, faceCentreArr):
    """
    Maps the list of faces off each cell contained in cellFaceArr with the 
    coordinate at the centre of each face in faceCentreArr. Each entry of 
    the lists in cellFaceArr corresponds to the index of faceCentreArr.
    This information is then used to compute the coordinate
    of each cell centre. 
    
    The list of faces belonging of each cell centre is an array of lists of
    variable length.
    """
    storage = np.empty((len(cellFaceArr), 3), dtype = float)
    
    tempStorage6 = np.empty((6, 3), dtype = float)
    tempStorage5 = np.empty((5, 3), dtype = float)
    tempStorage4 = np.empty((4, 3), dtype = float)
    tempStorage3 = np.empty((3, 3), dtype = float)
    
    
    for cell_ind, cell in enumerate(cellFaceArr):
        
        lCell = len(cell) # stupid but it works
        if lCell == 6:
            tempStorage = tempStorage6
        elif lCell == 5:
            tempStorage = tempStorage5
        elif lCell == 4:
            tempStorage = tempStorage4
        elif lCell == 3:
            tempStorage = tempStorage3
            
        for ind in range(lCell):
            
            tempStorage[ind,:] = faceCentreArr[cell[ind],:]
        storage[cell_ind,:] = 0.5*(tempStorage.min(axis=0) +
                                   tempStorage.max(axis=0))
        
    return storage

def spanAvgDict(cellArr, xRegion=[]):
    spanDict = {}
    for cell_ind, cell in enumerate(cellArr):
        currCell = tuple(cell[0:2])
        if len(xRegion) > 0:
            if not (cell[0:1] <= max(xRegion) and
                    cell[0:1] >= min(xRegion)):
                continue
        if currCell not in spanDict:
            spanDict[currCell] = [cell_ind]
        else:
            spanDict[currCell] += [cell_ind]
    return spanDict

def streamAvgDict(cell2DArr):
    streamDict = {}
    for cell_ind, cell in enumerate(cell2DArr):
        currCell = cell[1]
        if currCell not in streamDict:
            streamDict[currCell] = [cell_ind]
        else:
            streamDict[currCell] += [cell_ind]
    return streamDict

        

def spanAvgMesh(cellArr, spanAvgDict):
    
    storage = np.empty((len(spanAvgDict), 2), dtype=float)
    
    for span_ind, span in enumerate(spanAvgDict):
        cell_ind = spanAvgDict[span][0]
        storage[span_ind] = cellArr[cell_ind, :2]
        
    return storage

def averagedGrid(cell2DArr, streamAvgDict):
    
    storage = np.empty((len(streamAvgDict), 1), dtype=float)
    
    for stream_ind, stream in enumerate(streamAvgDict):
        cell_ind = streamAvgDict[stream][0]
        storage[stream_ind] = cell2DArr[cell_ind,1]
        
    return storage


# Classes
#------------------------------------------------------------------------------
class Mesh():
    '''
    The foamMesh class' purpose is to
        * read the openfoam formatted mesh files, 
        * derive additional information to obtain a list of the cell centre
          coordinates.
        * Output a 2d grid list of cell centres.
    Input:
        caseLocation: string containing the path to the case root folder
                      If left blank, current directory is assumed.
                      
    '''
                    
    def __init__(self, caseLocation = os.getcwd(), xRegion = []):
        self.caseLocation = caseLocation
        self.header = readGridHeader(caseLocation)
        self._pointsFileLocation = os.path.join(caseLocation,'constant',
                            'polyMesh', 'points')
        self._facesFileLocation = os.path.join(caseLocation,'constant',
                            'polyMesh', 'faces')
        self._ownerFileLocation = os.path.join(caseLocation,'constant',
                            'polyMesh', 'owner')

        self.pointCount = self.header['points']    
        self.faceCount = self.header['faces']
        self.cellCount = self.header['cells']
        
#    def cellCentres(self):
#        caseLocation = self.caseLocation
        
        self.pointArr = fetchPointArr(self.pointCount,self._pointsFileLocation)
        self.faceArr = fetchFaceArr(self.faceCount, self._facesFileLocation)
        self.facePointArr = calcFacePointArr(self.faceArr, self.pointArr)
        
        self.cellFaceArr = calcCellFaceArr(self.faceCount, self.cellCount, 
                                    self._ownerFileLocation)
        self.cellArr = calcCellArr(self.cellFaceArr, self.facePointArr)
        
        self.spanDict = spanAvgDict(self.cellArr, xRegion)
        
        self.cell2DArr = spanAvgMesh(self.cellArr, self.spanDict)
        
        if  len(xRegion) > 0:
            self.cell2DArr = self.cell2DArr[self.cell2DArr[:,0] <= max(xRegion)]


        self.streamDict = streamAvgDict(self.cell2DArr)
        
        self.averagedGrid = averagedGrid(self.cell2DArr, self.streamDict)
        
    
    def __call__(self):
        return self.cellArr
    
    
    def write(self, location):
        """
        Output cellArr to a txt file at a specified location
        """
        np.savetxt('location', self.cellArr2D)
    
    def writeBin(self, location):
        """
        Output cellArr to a binary file at a specified location
        """
        self.cellArr2D.tofile(location)
    
    
    def dump(self, location):
       """
       Output all mesh data into a specified location
       """
       pass


    def refresh(self):
        """
        Restart the class constructor in case any information has changed
        """
        
        self.__init__(self, location = os.getcwd())


class Field():
    """
    The Fields class is initialized by inputting:
        * The list/array of cells
            * array 
            * csv format
        * reading a json file containing the location both the times and fields
          to be processed
        * The location of the root case file.
    """
 
    def fetchField(self):        
        """
        Parses the field file and returns a numpy array of the field data
        input: None, depends on the header dictionary created at __init__
        ouput: fieldData array if store == False
        """
        header = self.header
        
        findFieldList =  False
        storage = np.zeros((header['cells'],
                            self.fieldTypeDict[header['fieldType']]),
                            dtype=float)
        
        with open(header['fieldLocation']) as file:
            for line in file:
                if line == '(\n':
                    findFieldList = True
                    ind = 0
                    continue
                if line == ')\n':
                    break
                if findFieldList:
    
                    storage[ind,:] = np.fromstring(line[1:-2], sep=' ')
                    ind += 1
            self.fieldData = storage
        
    def spanwiseAvg(self, spanAvgDict, field = None):
        """
        Spatial averaging of the field in the spanwise direction
        Input:
            * FieldArr: 3D field Array
            * spanAvgDict: dictionary conaining the list of cells corresponding
            to each unique value of a pair of the streamwise and vertical
            coordinates.
        """        
        field = self.fieldData

        header = self.header
        arrayWidth = self.fieldTypeDict[header['fieldType']]
        
        storage = np.empty((len(spanAvgDict), arrayWidth), dtype=float)

        for span_ind, span in enumerate(spanAvgDict):
            smallStorage = np.zeros((1, arrayWidth), dtype=float)

            for cell_ind in spanAvgDict[span]:
                
                smallStorage += field[cell_ind]

            storage[span_ind] = smallStorage/len(spanAvgDict[span])
         
        self.fieldData2D =  storage
    
    #@jit(nopython=True, parallel=True)
    def extractLine(self, x, cellArr2D, store = True):
        """
        """
        self._x = x
        field2D = self.fieldData2D
        arrayWidth = self.fieldTypeDict[self.header['fieldType']]

        header = self.header
        arrayWidth = self.fieldTypeDict[header['fieldType']]
        # Extract the a position array with the unique values of the x-cord
        uniqueXCord = np.unique(cellArr2D[:,0])
        loc_x1 = len(uniqueXCord[uniqueXCord <= x]) - 1
        loc_x2 =  loc_x1 +1 
        x1 = uniqueXCord[loc_x1]
        x2 = uniqueXCord[loc_x2]
        
        self._y = cellArr2D[cellArr2D[:,0] == x1][:,1]
        #X2 = cellArr2D[cellArr2D[:,0] == x2]
        
        field1Arr = field2D[cellArr2D[:,0] == x1]
        field2Arr = field2D[cellArr2D[:,0] == x2]
                
        xFrac = (x - x1)/(x2-x1)
        
        resultArr = np.zeros((len(self._y), arrayWidth + 1), dtype=float)
        
        interpolatedArr = xFrac*(field2Arr - field1Arr) + field1Arr

        resultArr[:,0] = self._y
        resultArr[:,1:] = interpolatedArr

        if store:
            self.lineData = resultArr
        else:
            return resultArr



    def writePlotData(self, lineData = [] ,saveLocation=os.getcwd(),labelStyle = 'uv',
                      decompose=True, positionLabel=True, format='%.6e'):
        """
        Writes the extracted averaged data into .xy files
        """
        # if postionLabel is True add a position label to the filename
        if positionLabel:
            xLabel = '_x'+str(self._x).replace('.','p')
        else: 
            xLabel = ''
        
        if len(lineData) == 0:
            lineData = self.lineData 
        
        header = self.header
        field = header['field']
        fieldType = header['fieldType']
        if decompose:
            
            if labelStyle == 'uv':
                labelList=self._uvLabelDict[fieldType]
            else:
                labelList=self._xyLabelDict[fieldType]
                
            width = self.fieldTypeDict[fieldType]

            for ind in range(width):
                plotData = np.vstack((lineData[:,0],
                                 lineData[:,ind+1])).transpose()
                label = labelList[ind]
                np.savetxt(os.path.join(saveLocation, field+label+xLabel+'.xy'),
                        plotData, fmt=format)
        else:
            np.savetxt(os.path.join(saveLocation, field+xLabel+'.xy'), lineData, fmt=format)
            
    def averagedField(self, field2DArr, streamDict, averagedGrid, store=True):
        """
        
        """
        header = self.header
        arrayWidth = self.fieldTypeDict[header['fieldType']]

        storage = np.empty((len(streamDict), arrayWidth + 1), dtype=float)
        storage[:,0] =  self._y

        for stream_ind, stream in enumerate(streamDict):
            
            smallStorage = np.zeros((1, arrayWidth), dtype=float)

            for cell_ind in streamDict[stream]:
                
                smallStorage += field2DArr[cell_ind]
            storage[stream_ind,1:] = smallStorage/len(streamDict[stream])
            
        if store:
            self.avgLineField =  storage
        else:
            return storage
                
    
    def __init__(self, time, field, caseLocation = os.getcwd()):
        
        if isinstance(time, (int, float)):
            time  = str(time)
            
        self.fieldLocation = os.path.join(caseLocation, time, field)
        #header dict wil be used throughout
        self.header = readFieldHeader(self.fieldLocation)
        self.header['caseLocation'] = caseLocation
        self.header['fieldLocation'] = self.fieldLocation
        self.caseLocation = caseLocation
        self.fieldTypeDict = {'volScalarField':1, 'volVectorField':3,
                          'volSymmTensorField':6, 'volTensorField': 9}
        self._xyLabelDict = {'volScalarField':'',
                                 'volVectorField':['x','y','z'],
                                 'volSymmTensorField':['xx','xy','xz',
                                                       'yy', 'yz','zz'],
                               'volTensorField': ['xx','xy','xz',
                                                  'xy', 'yy','yz',
                                                  'zx', 'zy','zz']}
        self._uvLabelDict = {'volScalarField':'',
                                 'volVectorField':['u','v','w'],
                                 'volSymmTensorField':['uu','uv','uw',
                                                       'vv', 'vw','ww'],
                               'volTensorField': ['uu','uv','uw',
                                                  'vu', 'vv','vw',
                                                  'wu', 'wv','ww']}
    def __repr__(self):
        pass
        #XX, XY, XZ, YY, YZ, ZZ 
#        self.cellCentreArr = cellCentreArr
#        self.fieldArr = self._fetchFields(self.cellCount, fieldClass = 'volVectorField',
#                                    location = caseLocation + '/83.2/UMean' )


        
        
        
        
        
        
    
if __name__ == '__main__':

    '''
caseLocation = '/home/leo/projects/channel180g90fixPress'
    mesh = foamMesh(caseLocation)
    np.savetxt('cell2DArr.txt',mesh.cell2DArr)
#    mesh.cellArr2D.tofile('2DArrTest.txt')
#    points, faces, cells = mesh.pointCount, mesh.faceCount, mesh.cellCount
#    pointArr = mesh.fetchPointArr(mesh.pointCount)
#    faceArr = mesh.fetchFaceArr(mesh.faceCount)
#    facePointeArr = mesh.calcFacePointArr(faceArr, pointArr)
#    cellFaceArr = mesh.calcCellFaceArr(mesh.faceCount, mesh.cellCount)
#    mesh.cellCentres()
#    cellCentreArr = mesh.cellArr
    spanDict = mesh.spanDict
    cellCentreArr2D = mesh.cell2DArr
#    
    UMean = Fields(83.2, 'UMean', caseLocation=caseLocation)
    UMean.fetchField()
#    UMeanArr = UMean.fieldArr
    UMean.spanwiseAvg(UMean.fieldData, spanDict)


    UMean.extractLine(3.2, UMean.fieldData2D, mesh.cell2DArr)  
    
    UMean.writePlotData(UMean.lineData, saveLocation='test')
    
    

#    
#    message = "The values of the 2D field are not consistent with the 3D field."
#    assert UMeanArr2D[:,0].min() >= UMeanArr[:,0].min(), message
#          
#    
#    assert UMeanArr2D[:,0].max() <= UMeanArr[:,0].max(), message
    
    '''
