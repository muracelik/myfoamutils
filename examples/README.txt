The purpose of this document is to describe the documents contained in the
example folder of the pyFoamUtils python utility

--------------------------------------------------------------------------
LIST OF DOCUMENTS

* preExample.ipynb

Usage examples of the pyfoamutils.pre sub module contained inside a jupyter
notebook. The utility outputs blockMeshDict files contained in the gridsFolder
directory

* postExample.ipynb

Usage examples of the pyfoamutils.post sub module contained inside a jupyter
notebook. This example depends on the `channel18090FixPress` OpenFOAM case
directory. Output plot files are contained insde the graphs folder

* postBFS.ipynb

Usage examples of the pyfoamutils.post sub module contained inside a
jupyter notebook. The OpenFOAM case directory for this example is not present
due to its large size. The example showcases the xRegion feature of the post
processing tool as well as the spatial (streamwise and spanwise) averaging.

Resulting plot files are contained inside the plots directory.

* postBFS.html

html version of postBFS.ipynb

* postBFS.py

python script version of postBFS.ipynb. May be executed with the command:

    python postBFS.py
